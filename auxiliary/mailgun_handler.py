import logging
import requests
import string, logging, logging.handlers

class MailgunHandler(logging.handlers.BufferingHandler):

    def __init__(self, api_server_name, api_key, sender, recipients, subject, capacity):
        logging.handlers.BufferingHandler.__init__(self, capacity)
        # run the regular Handler __init__
        logging.Handler.__init__(self)
        self.api_url = "https://api.mailgun.net/v3/{0}/{1}".format(api_server_name,"messages")
        self.api_key = api_key
        self.sender = sender
        self.recipients = recipients
        self.subject = subject

    def flush(self):
        if len(self.buffer) > 0:
            # record is the log messages
            for recipient in self.recipients:
                record = ''
                for logentry in self.buffer:
                    s = self.format(logentry)
                    record = record + s + "\r\n"
                data = {
                    "from": self.sender,
                    "to": recipient,
                    "subject": self.subject,
                    "text": record
                }
                requests.post(self.api_url, auth=("api", self.api_key), data=data)
            self.buffer = []