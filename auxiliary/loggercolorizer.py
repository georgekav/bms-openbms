from logging import *
from mailgun_handler import MailgunHandler
from colorlog import ColoredFormatter
from conf_general import CURRENT_IP, MAILGUN_ACCESS_KEY, MAILGUN_SERVER_NAME, EMAIL_LOGGING_RECIPIENT

def getColorLogger(level=INFO, name=None,
                   email_sender='root@logging', activate_email_logging=False, send_every=10, send_email_level=ERROR,
                   filename=None):
    """ Get and initialize a colourised logging instance if the system supports
    it as defined by the log.has_colour

    :param name: Name of the logger
    :param level: logger global level
    :param email_sender: the senser of the email of logging
    :param activate_email_logging: enable the email logging at error
    :param send_every: every how many log entries to send email
    :param send_email_level: the logger level for sending email
    :param filename: if set, the logger will write also to a file
    :return: Logger instance
    """
    log = getLogger(name)

    for loghandler in log.handlers:
        log.removeHandler(hdlr=loghandler)

    colformater = ColoredFormatter(
        fmt="%(log_color)s%(levelname)-8s %(asctime)s   %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        reset=True,
        log_colors={
                'DEBUG':    'bold_black',
                'INFO':     'white',
                'WARNING':  'yellow',
                'ERROR':    'red',
                'CRITICAL': 'bold_red',
        },
        secondary_log_colors={},
        style='%'
    )

    emailformater = Formatter(
        fmt="%(levelname)-8s %(asctime)s   %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    fileformater = emailformater

    # Only enable colour if support was loaded properly
    console_handler = StreamHandler()
    console_handler.setLevel(level=level)
    console_handler.setFormatter(fmt=colformater)
    log.addHandler(hdlr=console_handler)

    if activate_email_logging:
        email_handler = MailgunHandler(api_key=MAILGUN_ACCESS_KEY, api_server_name=MAILGUN_SERVER_NAME,
                                       sender=email_sender, recipients=[EMAIL_LOGGING_RECIPIENT], subject='Logging at {0}'.format(CURRENT_IP), capacity=send_every)
        email_handler.setLevel(level=send_email_level)
        email_handler.setFormatter(fmt=emailformater)
        log.addHandler(hdlr=email_handler)

    if filename:
        file_handler = FileHandler(filename=filename)
        file_handler.setLevel(level=level)
        file_handler.setFormatter(fmt=fileformater)
        log.addHandler(hdlr=file_handler)

    log.setLevel(level)
    log.propagate = 0 # Don't bubble up to the root logger
    return log

#Old colorizer
# import time, datetime
# try:
#     from colorama import Fore, Back, init, Style
#
#     class ColourStreamHandler(StreamHandler):
#         """ A colorized output SteamHandler """
#
#         # Some basic colour scheme defaults
#         colours = {
#             'DEBUG' : Fore.CYAN,
#             'INFO' : Fore.GREEN,
#             'WARN' : Fore.YELLOW,
#             'WARNING': Fore.YELLOW,
#             'ERROR': Fore.RED,
#             'CRIT' : Back.RED + Fore.WHITE,
#             'CRITICAL' : Back.RED + Fore.WHITE
#         }
#
#         @property
#         def is_tty(self):
#             """ Check if we are using a "real" TTY. If we are not using a TTY it means that
#             the colour output should be disabled.
#
#             :return: Using a TTY status
#             :rtype: bool
#             """
#             try: return getattr(self.stream, 'isatty', None)()
#             except: return False
#
#         def emit(self, record):
#             try:
#                 dateandtime = datetime.datetime.fromtimestamp(time.time())
#                 userfriendlytime = dateandtime.strftime('%H')  + ':' + dateandtime.strftime('%M')  + ':' + dateandtime.strftime('%S')
#                 userfriendlydate = dateandtime.strftime('%d') + dateandtime.strftime('%b')
#                 message = self.format(record)
#                 extendedmessage = '[{0} @ {1}]   {2}'.format(userfriendlydate,userfriendlytime, message)
#                 if not self.is_tty:
#                     self.stream.write(extendedmessage)
#                 else:
#                     self.stream.write(self.colours[record.levelname] + extendedmessage + Style.RESET_ALL)
#                 self.stream.write(getattr(self, 'terminator', '\n'))
#                 self.flush()
#             except (KeyboardInterrupt, SystemExit):
#                 raise
#             except:
#                 self.handleError(record)
#
# except:
#     #revert to simple StreamHandler
#     class ColourStreamHandler(StreamHandler):
#         def __init__(self):
#             super(ColourStreamHandler, self).__init__()