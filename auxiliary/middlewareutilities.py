__author__ = 'Georgios Lilis'
import sys
import requests
import logging
from conf_general import CURRENT_IP
from conf_openbms import OPENBMS_IP, OPENBMS_PORT, APIsecKey

def get_middleware_details(midid):
    """Returns the middleware dictionary for use by the daemons"""
    geturl = 'http://{0}:{1}/API/middleware/{2}'.format(OPENBMS_IP,OPENBMS_PORT, midid)
    r = requests.get(geturl)
    if r.status_code == 200: # Otherwise it means openBMS doesnt work
        middleware = r.json()[0]['fields']
        if middleware['IP'] == '255.255.255.255':
            logging.info('Reporting back the CURRENT_IP {0} to replace default 255.255.255.255'.format(CURRENT_IP))
            set_middleware_ip(midid=midid, midIP=CURRENT_IP)
            middleware['IP'] = CURRENT_IP
        if middleware['IP'] == '127.0.0.1':
            logging.info('The middleware address is configured as 127.0.0.1. Binding the socket to the default IP of openBMS {0}'.format(OPENBMS_IP))
            middleware['IP'] = OPENBMS_IP
        return middleware
    elif r.status_code == 404 and r.content:  # Server replied but not found the midid
        logging.error('Failed to find in openBMS the middleware: {0}'.format(midid))
        sys.exit()
    else:  # Server did not reply, maybe offline
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()

def get_middleware_objects(midid):
    """Returns the connected objects to the middleware"""
    geturl = 'http://{0}:{1}/API/middleware_objects/{2}'.format(OPENBMS_IP,OPENBMS_PORT, midid)
    r = requests.get(geturl)
    if r.status_code == 200: # Otherwise it means openBMS doesnt work
        middlewareobjects = list()
        for midobj in r.json():
            middlewareobjects.append(midobj['fields'])
        return middlewareobjects
    elif r.status_code == 404 and r.content:  # Server replied but not found the midid
        logging.error('Failed to find in openBMS the middleware: {0}'.format(midid))
        sys.exit()
    else:  # Server did not reply, maybe offline
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()

def get_middleware_refs(midid):
    """Returns the connected refs to the middleware"""
    geturl = 'http://{0}:{1}/API/middleware_refs/{2}'.format(OPENBMS_IP,OPENBMS_PORT, midid)
    r = requests.get(geturl)
    if r.status_code == 200: # Otherwise it means openBMS doesnt work
        return r.json()
    elif r.status_code == 404 and r.content:  # Server replied but not found the midid
        logging.error('Failed to find in openBMS the middleware: {0}'.format(midid))
        sys.exit()
    else:  # Server did not reply, maybe offline
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()

def set_middleware_ip(midid, midIP):
    """Report the IP back to openBMS for autoconfiguration"""
    posturl = 'http://{0}:{1}/API/middleware_reportip/{2}/?IP={3}&api_sec_key={4}'.format(OPENBMS_IP,OPENBMS_PORT, midid, midIP, APIsecKey)
    r = requests.post(posturl)
    if r.status_code == 200: # Otherwise it means openBMS doesnt work
        return True
    elif r.status_code == 404 and r.content:  # Server replied but not found the midid
        logging.error('Failed to find in openBMS the middleware: {0}'.format(midid))
        sys.exit()
    else:  # Server did not reply, maybe offline
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()