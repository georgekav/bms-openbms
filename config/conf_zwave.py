__author__ = 'Georgios Lilis'
from conf_general import PROJECT_DIR
from collections import namedtuple


#-----------------------ZWAVE-----------------------#
WAIT_TIME_ZWAVE = 1.0 #1sec to be sure
FTDI_SERIAL = 'FTZWAVE0'
SERIAL = 'COM46'
IS_USBZWAVE_MODULE = False #Should be set when using a zwave usb stick
dev_conf_tuple = namedtuple('USB_PARAM', ('SerialDevice', 'BaudRate', 'Databits', 'Stopbits', 'FlowControl', 'Parity', 'ReadTimeout', 'WriteTimeout'))
ZWAVE_USB_PARAM = dev_conf_tuple(SERIAL, 115200, 8, 1, 0x0000, 0, WAIT_TIME_ZWAVE, -1)  # Zwave parameters, 1s timeout
COMM_MAX_RETRIES = 10

ZWAVE_HEADER_SOF = '\x01'
ZWAVE_HEADER_ACK = '\x06'
ZWAVE_HEADER_NACK = '\x15'
ZWAVE_HEADER_CAN = '\x18'
ZWAVE_DESIRED_CONF = (('FF', '01', 'FF'),  # Do a full reset to configuration
                      ('02', '01', '01'),  # Battery insert act
                      ('03', '02', '00f0'),  # Timeout of no motion detection, 240sec
                      ('04', '01', '01'),  # Enable PIR
                      ('05', '01', '02'),  # PIR detection send BINARY packet
                      ('65', '04', '000000e1'),  # Send all the sensors to group 1, e0 to remove battery report
                      ('6f', '04', '000000f0')  # Set the sensor time interval at 4minutes
                      )
ZWAVE_SUPPORTED_SENSORS = ('Air temperature', 'Luminance', 'Humidity', 'PIR', 'Ultraviolet', 'Power', 'Battery Level')  # Only for internal Zwave use
ZWAVE_SENSORS_RESOLUTION = dict(((ZWAVE_SUPPORTED_SENSORS[0], 0.1),
                                 (ZWAVE_SUPPORTED_SENSORS[1], 1),
                                 (ZWAVE_SUPPORTED_SENSORS[2], 1),
                                 (ZWAVE_SUPPORTED_SENSORS[3], -1),
                                 (ZWAVE_SUPPORTED_SENSORS[4], 1),
                                 (ZWAVE_SUPPORTED_SENSORS[5], 0.01),
                                 (ZWAVE_SUPPORTED_SENSORS[6], 1)))

ZWAVE_SENSORS_ERROR_DELTA = dict(((ZWAVE_SUPPORTED_SENSORS[0], 20),
                                  (ZWAVE_SUPPORTED_SENSORS[1], 1100),
                                  (ZWAVE_SUPPORTED_SENSORS[2], 20),
                                  (ZWAVE_SUPPORTED_SENSORS[3], 1),
                                  (ZWAVE_SUPPORTED_SENSORS[4], 20),
                                  (ZWAVE_SUPPORTED_SENSORS[5], 5000),
                                  (ZWAVE_SUPPORTED_SENSORS[6], 100))) #It can go from uncharged to 100% charged

ZWAVE_SENSORS_VALID_RANGE = dict(((ZWAVE_SUPPORTED_SENSORS[0], (-20, 50)),
                                  (ZWAVE_SUPPORTED_SENSORS[1], (0, 1000)),
                                  (ZWAVE_SUPPORTED_SENSORS[2], (10, 90)),
                                  (ZWAVE_SUPPORTED_SENSORS[3], (0, 1)),
                                  (ZWAVE_SUPPORTED_SENSORS[4], (0, 15)),
                                  (ZWAVE_SUPPORTED_SENSORS[5], (0, 12000)), #12kW
                                  (ZWAVE_SUPPORTED_SENSORS[6], (0, 100))))


#-----------------ZWAVE Daemon----------------#
BUFFERNAME = PROJECT_DIR + '/backend/daemons/zwavebuffer.bin'

ZWAVE_MID_ID = 2