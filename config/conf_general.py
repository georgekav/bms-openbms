__author__ = 'Georgios Lilis'
import sys, os, socket, logging

PROJECT_DIR = os.path.abspath(__file__ + "/../../") #Elab-bms folder
#2 levels folder walk and adding to path
for f in os.listdir(PROJECT_DIR):
    if os.path.isdir(os.path.join(PROJECT_DIR,f)) and f not in ['builds', 'deprecated', 'measures', '.idea', '.git']:
        subfolder = os.path.join(PROJECT_DIR,f)
        sys.path.append(subfolder)
        for subf in os.listdir(subfolder):
            if os.path.isdir(os.path.join(subfolder,subf)) and subf not in ['.idea', '.git']:
                subsubfolder = os.path.join(subfolder,subf)
                sys.path.append(subsubfolder)

import ipgetter_custom #Need to be Below the path inclusion

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(('8.8.8.8', 80))
CURRENT_IP = s.getsockname()[0]
s.close()
EXTERNAL_IP = ipgetter_custom.myip()
if EXTERNAL_IP != CURRENT_IP:
    logging.warning('Be advised, external IP does not much the IP of the interface')


DAEMON_DEPENDENCIES = ['zmq', 'lxml']

#================================================#

#-----------------------Backend API-----------------------#
#SENSORS_API_TYPES = ('TEM', 'LUM', 'HUM', 'PRE', 'UVB', 'P', 'BAT') #Should be in sync with ZWAVE_SUPPORTED_SENSORS, and also with models.py
SENSORS_API_TYPES = ('TEM', 'LUM', 'HUM', 'PRE', 'UVB', 'EMPTY', 'BAT') #temporary patching the bug of P found in "sensor"
LOADS_API_TYPES = ('P', 'Q', 'S', 'PF')
ACTUATOR_API_TYPES = ('RELAY', 'DIMMER', 'MOD') #The first two from PLC config.py
MODE_API_TYPE = ACTUATOR_API_TYPES[2]

PUB_MSG_ORIGIN = ('PLC', 'ZWAVE', 'TCP')
#STATUS is for actuator updates
PUB_MSG_TYPE = ('STATUS', 'MEASURE', 'LOAD_REC', 'USER_LOC', 'USER_REC', 'VMID_PING') #DO NOT CHANGE THE ORDER UNDER ANY WAY!!!
#================================================#
LOGGER_LVL = 'INFO'

#Mailgun configuration
MAILGUN_ACCESS_KEY = 'key-b878d8fc1ed30f214b5a28f5892c0f38'
MAILGUN_SERVER_NAME = 'sandbox9ba29674db35438688e8321fc35cf7d2.mailgun.org'
EMAIL_LOGGING_RECIPIENT = 'georgios.lilis@epfl.ch'
