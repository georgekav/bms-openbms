__author__ = 'Georgios Lilis'
#------------------ZMQ------------------#
#These are the MID_ID that would be queried the openBMS API
PLC_MID_ID = []  # PLC daemons to collect
ZWAVE_MID_ID = [] # ZWAVE daemons to collect
DB_MID_ID = [] # DB daemons to report

# The time to wait for the DB daemon to respond. Increase the value if the DB daemon is loaded.
# If the timeout is low and DB daemon is in fact working but takes time to reply, this can create a cascade effect
# of messages waiting in queue and thus when prosessed will have duplicaet entrys and geometrically increase
# repeating workload; the accudaemons would contitue to bombard with entries
DB_POLL_TIMEOUT = 5000