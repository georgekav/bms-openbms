__author__ = 'Georgios Lilis'
from conf_openbms import OPENBMS_IP

#----------------REAL_TIME_SERVER----------------#
#This should be the same with django server at the moment since they share the sqlite db
RT_SERVER_IP = OPENBMS_IP
RT_SERVER_PORT = '8000'

RTSERVER_DEPENDENCIES = ['zmq', 'tornado']


WEBSOCKET_ZMQ_MSG_TYPE = "ZMQ_MSG" #for messages arriving through the zmq loop
WEBSOCKET_WEB_MSG_TYPE = "WEB_MSG" #for messages arriving through the websockets loop

#Auxiliary address space for websockets PUB_ID (the rest is normally reserved for room ids (openBMS)
PUB_ID_BROADCAST = 0 #also used when registering to all
PUB_ID_FRONTEND = -1 #events originating from frontend (e.g. register PUB_ID)
PUB_ID_BMS_USER_REC = -2 #when no room 'attribution' is possible. In other cases the websocket message will have the room.
PUB_ID_EMS = -3 #event by the EMS
PUB_ID_BMS_USER_LOC = -4 #for location updates
PUB_ID_VMID_PING = -5 #the ping timing updates publisher from the vMid
PUB_ID_OTHER = -999 #misc id when we dont care for the publisher
EXTRA_PUB_ID_LIST = [PUB_ID_BMS_USER_REC, PUB_ID_BMS_USER_LOC, PUB_ID_EMS, PUB_ID_FRONTEND, PUB_ID_VMID_PING] #used by broadcast registrar to complement the room list