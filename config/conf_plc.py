__author__ = 'Georgios Lilis'
from conf_general import PROJECT_DIR
from collections import namedtuple

#-----------------------PLC-----------------------#
WAIT_TIME_PLC = 0.300 #200ms timeout
FTDI_SERIAL = 'FTPLUG0'
SERIAL = 'COM27'
NETWORK_DISCOVERY_MAX_ADDRESS = 0xC0
dev_conf_tuple = namedtuple('USB_PARAM', ('SerialDevice', 'BaudRate', 'Databits', 'Stopbits', 'FlowControl', 'Parity', 'ReadTimeout', 'WriteTimeout'))
PLC_USB_PARAM = dev_conf_tuple(SERIAL, 38400, 8, 1, 0x0000, 0, WAIT_TIME_PLC, -1)  # PLC parameters

PLC_POWER_RESOLUTION = 1

#They are used for status requests from plc daemon
PLC_ACTUATOR_TYPES = ('RELAY', 'DIMMER')

#They are used for CMD to PLC
RELAY_CMD = ('ON', 'OFF', 'TOGGLE')
DIMMER_CMD = 'DIM'

HEADER_E5 = '\xE5'
HEADER_ACK = '\xE1'
HEADER_NACK = '\xE2'
VERSION = '\x01'
ACK = HEADER_ACK + VERSION + '\x00' + '\xEC'  # '\xE1\x01\x00\xEC'
NACK = HEADER_NACK + VERSION + '\x00' + '\x26'  # '\xE2\x01\x00\x26'\
PLC_ACK = 'PLC_ACK'
SYNCPLC_CMD = 'SPOWER'
PLC_REQ_MSG_TYPE = dict((('Measure', '\x01'), ('Command', '\x02'), ('Config', '\x05')))
PLC_REQ_PAYLOAD = dict((
    #Forward dict for requests. They are used by the plc API to respect the protocol.
    #Command
    (RELAY_CMD[1], '\x03\x00\x00'),
    (RELAY_CMD[0], '\x03\x00\x01'),
    (RELAY_CMD[2], '\x03\x00\x02'),
    (DIMMER_CMD, '\x04'),  # It needs the dim setting
    (SYNCPLC_CMD, '\xA0\x01\x00'),
    (PLC_ACK, '\xB9'),
    ('FirmwareREQ', '\x01\x00\x56'),
    ('ConfREQ', '\x60\x04'),

    #Measure used by the old sync PLC plugs (obsolete)
    ('PowerW', '\x07'),
    ('PowerkW', '\x08'),
    ('PowerMW', '\x09'),
    ('Spf', '\xA1'),
))

PLC_CONFIG_CMD = dict((  # The type of configuration of eeprom type commands
                         ('GET', '\x00'),
                         ('AND', '\x01'),
                         ('OR', '\x02'),
                         ('SET', '\x03'),
                         ))

#used by the old sync PLC plugs (obsolete)
PLC_REP_PAYLOAD = dict((  # Reverse dict for plc protocol replies
                          ('\x07', 'P_0_2'),
                          ('\x08', 'P_3_2'),
                          ('\x09', 'P_6_2'),
                          ('\xA1', 'Spf_0_6'),
                          ('\xB9', PLC_ACK),
                          ))

#Used for the REP especialy in the new async plugs
PLC_DATA_TYPES = {
    0x00: 'None',
    0x01: 'Measurement',
    0x02: 'Command',
    0x03: 'Broadcast',
    0x04: 'Parameter',
    0x05: 'Config',
    0x06: 'Error',
    0x07: 'Reprog',
    0x08: 'Rfid'
}
PLC_UNITS = {
    0x00: (2, 'None'),
    0x01: (2, 'get_measure'),
    0x02: (2, 'get_parameter'),
    0x03: (2, 'io_state'),
    0x04: (2, 'dimming_state'),
    0x05: (2, 'num_clics'),
    0x06: (2, 'pulse_duration'),
    0x07: (2, 'power_watt'),
    0x08: (2, 'power_kilowatt'),
    0x09: (2, 'power_megawatt'),
    0x0A: (2, 'temp_aller'),
    0x0B: (2, 'temp_retour'),
    0x0C: (2, 'delta_t'),
    0x10: (2, 'flow_dm3-h'),
    0x12: (2, 'target_temp'),
    0x14: (2, 'blinds_pos'),
    0x15: (2, 'blinds_orient'),
    0x16: (2, 'version_number'),
    0x17: (2, 'launch_scene'),
    0x18: (2, 'scenes_version'),
    0x19: (2, 'unknown'),
    0x1A: (2, 'unknown'),
    0x1D: (2, 'module_id'),
    0x1E: (2, 'module_subnet'),
    0x1F: (2, 'module_channel'),
    0x20: (2, 'none'),
    0x21: (2, 'flash_session_start'),
    0x22: (2, 'firmware_sent'),
    0x23: (2, 'reflash_command'),
    0x24: (2, 'abort_reflash_session'),
    0x25: (2, 'flash_session_error'),
    0x26: (2, 'firmware_packet_missing'),
    0x27: (2, 'firmware_transfer_success'),
    0x28: (2, 'begin_flashing_neighbours'),
    0x29: (2, 'abort_flashing_neighbours'),
    0x2A: (2, 'neighbour_transfer_success'),
    0x2B: (2, 'reflash_neighbours_command'),
    0x2C: (2, 'reflash_single_remote'),
    0x2F: (2, 'force_reset'),
    0x30: (2, 'map_id'),
    0x34: (2, 'analog_out'),
    0x35: (2, 'analog_in'),
    0x40: (4, 'energy_watthour'),
    0x41: (4, 'none'),
    0x42: (4, 'volume_tenth_dm3'),
    0x51: (4, 'configure_scene'),
}
PLC_ERRORS = {
    0x00: (2, 'None'),
    0x01: (2, 'No answer'),
    0x02: (2, 'Short Circuit'),
    0x03: (2, 'Read error'),
    0x04: (2, 'High temperature'),
    0x05: (2, 'Watchdog reset'),
    0x06: (2, 'Software reset'),
    0x25: (2, 'Flash session error'),
    0x0A: (2, 'Flash session not started'),
}
PLC_FIRMWARES = {
    0x00: 'None',
    0x01: 'eSMART-Plug V1',
    0x02: 'eSMART-Plug',
    0x05: 'MBus-Heating',
}
PLC_BOOTLOADERS = {
    0x00: 'None',
    0x02: 'eSMART-Boot',
}
FLASH_ERRORS = {
    0x00: 'Missing Packet',
    0x01: 'Invalid Number of Packets',
    0x02: 'Invalid CRC',
    0x03: 'Failed reading firmware',
    0x04: 'Failed writing firmware',
    0x05: 'Firmware not ready',
    0x06: 'Received too many packets',
    0x07: 'Failed to write magic word',
    0x08: '(Warning) CRC Ongoing : request discarded',
    0x09: 'Invalid Magic word',
    0x0A: 'Programming session not started',
    0x0B: '(Warning) : firmware not ready',
    0x0C: 'Maximum number of retries reached',
}

#-----------------PLC Daemon----------------#
BUFFERNAME = PROJECT_DIR + '/backend/daemons/plcbuffer.bin'
DEFAULT_STAT = 0
STATUS_ON = 1
STATUS_OFF = 0
SYNC_SAMPLE_RATE = 120  # in sec

PLC_MID_ID = 1