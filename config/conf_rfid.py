__author__ = 'Georgios Lilis'
from collections import namedtuple

USERREC_MID_ID = 9

#-----------------------RFID-----------------------#
WAIT_TIME_RFID = 1.0 #1sec to be sure
FTDI_SERIAL = 'FTRFID0'
SERIAL = 'COM59'
dev_conf_tuple = namedtuple('USB_PARAM', ('SerialDevice', 'BaudRate', 'Databits', 'Stopbits', 'FlowControl', 'Parity', 'ReadTimeout', 'WriteTimeout'))
RFID_USB_PARAM_INIT = dev_conf_tuple(SERIAL, 57600, 8, 1, 0x0000, 0, WAIT_TIME_RFID, -1)  # RFID parameters, 1s timeout
RFID_USB_PARAM_HS = dev_conf_tuple(SERIAL, 521538, 8, 1, 0x0000, 0, WAIT_TIME_RFID, -1)  # High speed baud rate

TAG_LEFT_MAX = 30
TIME_TO_HIBERNATE = 0.2 #in sec
ECHO_TRIES = 2 #Tries before quiting the communication with the reader
SCANNED_PROTOCOLS = ['ISO14443A', 'ISO15693']
CALIBRATION_3_3_V = '070E03A100F8011800206060787F3F01'
NO_TAG_STRING = 'NOTAG'