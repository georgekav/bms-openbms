__author__ = 'Georgios Lilis'
from conf_general import PROJECT_DIR, CURRENT_IP

#----------------USED FOR TCP DAEMONS--------------#
#we need them hard defined in order to be able to start the django project before fetch them from DB
TCP_REMOTE_PORT = '51000'
MODE_API_TYPE = 'MOD'
MODE_CMD = 'MOD'
NETWORK_PREFIX = '128.178.19.'  #openBMS supports one subnet only
#-----------------TCP Daemon----------------#
BUFFERNAME = PROJECT_DIR + '/frontend/BmsAPI/tcpbuffer.bin'
STATUS_ON = 0
STATUS_SLEEP = 1
STATUS_OFF = 2
DEFAULT_STAT = STATUS_ON

TCP_MID_ID = 4