__author__ = 'Georgios Lilis'

#=================MAIN openBMS===================#
OPENBMS_IP = '128.178.19.163'
OPENBMS_PORT = '80'
OPENBMS_DEBUG = True

DJANGO_DEPENDENCIES = ['django', 'zmq', 'scipy']

#View configuration
DJANGO_TIMEOUT_TIMESERIES = 60000
DJANGO_TIMEOUT_LASTVALUE = 30000
DJANGO_TIMEOUT_ACTION = 5000
DJANGO_TIMEOUT_STATUS = 15000

DJANGO_ENERGYRES=60 #the resolution of the intergrator of energy
DJANGO_UPDATE_INTERVAL = 15 * 60 * 1000 #the Update Interval Of The Last Points
DJANGO_UPDATE_GRAPH_INTERVAL = 10 * 1000 #the Update Interval Of The Graph, The Overhead Is To The Client
DJANGO_MAX_SAMPLES = 1000 #max recommended number of samples for graphs to calculate the res

DJANGO_MAX_POWERBAR = 2500 #max value of the power bar
DJANGO_MAX_ENERGYGAUGE = 1500
DJANGO_MAX_COSTGAUGE = 45
DJANGO_MAX_CO2GAUGE = 35

#This one is defining the threshold of resolution where the interpollation with change to mean value calculation
#If the resolution is too low (for example an hour) then infact we need averages and resample since the data is for sure
#in much higher resoltuion
INTERPOLATION_TYPE_SWITCH_POINT = 60
#backendAPI timeout
BACKENDAPI_BASE_TIMEOUT = 2000

APIsecKey = 'asdrrt12'
GOOGLE_API_KEY = 'AIzaSyCya9pvCH1qzxxLfvTYLsFvED2eHSP7w74'