__author__ = 'Georgios Lilis'
import os
from conf_general import PROJECT_DIR

#-------------------DB Daemon-------------------#
# For use in DBdaemon and datacollector
DB_STORE_DIR = os.path.join(PROJECT_DIR,"measures")
DB_MID_ID = 3  #The DB middleware entry of this daemon

#------------------CSV SPECIFIC-----------------#
STORAGE_TYPE = 'CSV'
CSV_FIELDS = ('Time', 'Value')
LAST_ROWS_NUMBER = 5000 #number of rows to keep in memory of csvAPI, not all the csv file is kept, if more in the past is needed it fetches again.
CSV_FLUSH_INTERVAL = 50 #Number of collected rows before writing the csv files