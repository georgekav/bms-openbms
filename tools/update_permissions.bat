@ECHO off

git ls-files --stage | awk '{print $4}' | grep .sh | xargs git update-index --chmod=+x
git ls-files --stage | grep .sh

@echo:
ECHO Finished the setting the permissions
PAUSE