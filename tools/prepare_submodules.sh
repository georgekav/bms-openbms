#!/bin/sh
echo Changing directory to $HOME
cd $HOME
cd ./elab-bms
echo Synchronizing the submodules url based on the .gitmodules
git submodule sync
echo Initialize and update the submodules
git submodule update --init
