#!/bin/sh
echo Changing directory to $HOME
cd $HOME
cd ./elab-bms
echo Mirroring remote, any changes to HEAD are LOST!
git fetch origin
git reset --hard origin/master
git submodule foreach 'git fetch origin; git reset --hard origin/master'
