#!/bin/sh
echo Changing directory to $HOME
cd $HOME
cd ./elab-bms
echo Pulling new changes from remote for the current branch
git pull origin
git submodule foreach 'git pull origin'

