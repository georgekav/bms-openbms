#!/bin/sh
echo Changing directory to $HOME
cd $HOME
cd ./elab-bms
echo Updating the remote ref to master
git fetch origin master:master
echo Pulling new changes from remote for master
git pull origin master
#Reset the frontend binary
cd ./frontend
git checkout -- SmartBuilding.sqlite
cd ..
git submodule foreach 'git pull origin'
