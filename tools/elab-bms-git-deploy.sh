#!/bin/sh
echo 
echo Changing directory to $HOME
cd $HOME
echo "Begin the cloning of the elab-bms repository"
git clone https://git.epfl.ch/repo/elab-bms.git elab-bms
cd elab-bms

echo
echo -n "Clone also the frontend sources (y/n) > "
read choice
if [ "$choice" = "y" ] 
then
	git clone https://git.epfl.ch/repo/bms-frontend.git frontend
else
	echo "Skipping the frontend sources"
fi


echo
echo -n "Clone also the backend sources (y/n) > "
read choice
if [ "$choice" = "y" ] 
then
	git clone https://git.epfl.ch/repo/bms-backend.git backend
	echo
	echo -n "Clone also the esmart-loader (y/n) > "
	read choice
	if [ "$choice" = "y" ] 
	then
		cd ./backend/utilities/
		git clone https://bitbucket.org/georgekav/esmart-loader.git firmware-loader
		cd ../../
	fi
else
	echo "Skipping the backend sources"
fi

echo
echo
echo "Finished the cloning of repositories"
sleep 5