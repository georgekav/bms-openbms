@ECHO off
SETLOCAL ENABLEDELAYEDEXPANSION

REM new line
@echo: 
ECHO Begin the cloning of the elab-bms repository
git clone https://git.epfl.ch/repo/elab-bms.git elab-bms
cd elab-bms

@echo:
SET /P CHOICE="Clone also the frontend sources (Y/N)"
IF /I "%CHOICE%" == "Y" (	
	git clone https://git.epfl.ch/repo/bms-frontend.git frontend
) ELSE (
	ECHO Skipping the frontend sources
)
@echo:	
SET /P CHOICE="Clone also the backend sources (Y/N)"
IF /I "%CHOICE%" == "Y" (
	git clone https://git.epfl.ch/repo/bms-backend.git backend
	
	@echo:
	SET /P CHOICE="Clone also the esmart-loader (Y/N)"
	IF /I "!CHOICE!" == "Y" (
		cd ./backend/utilities/
		git clone https://bitbucket.org/georgekav/esmart-loader.git firmware-loader
		cd ../../
	) 
) ELSE (
	ECHO Skipping the backend sources
)
@echo:
@echo:
ECHO Finished the cloning of repositories
SLEEP 5