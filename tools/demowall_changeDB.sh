#!/bin/sh
zwave_mid="7"
plc_mid="5"
wall_db_mid="8"

echo Changing directory to $HOME
cd $HOME
cd ./elab-bms

sqlite3 ./frontend/SmartBuilding.sqlite "UPDATE BmsApp_sensor SET store_middleware_id=$wall_db_mid WHERE BmsApp_sensor.network_middleware_id=$zwave_mid or BmsApp_sensor.network_middleware_id=$plc_mid; SELECT COUNT(*) FROM BmsApp_sensor WHERE (BmsApp_sensor.network_middleware_id=$zwave_mid or BmsApp_sensor.network_middleware_id=$plc_mid) and BmsApp_sensor.store_middleware_id<>8;"
