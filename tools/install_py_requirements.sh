#!/bin/sh
echo Changing directory to $HOME
cd $HOME
echo Installing necessary python packages

echo pip is necessary
sudo apt-get install python-pip

echo Updating it
pip install -U setuptools

echo Installing the packages
pip install -r requirements.txt

