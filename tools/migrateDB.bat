@ECHO off

python ../../elab-bms/frontend/manage.py makemigrations
python ../../elab-bms/frontend/manage.py migrate

@echo:
ECHO Finished the migrations
PAUSE