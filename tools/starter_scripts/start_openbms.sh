#!/bin/sh
echo Changing directory to $HOME
cd $HOME
cd ./elab-bms/frontend
echo Starting openBMS server
gunicorn -c /root/elab-bms/config/gunicorn.conf SmartBuilding.wsgi:application

#gunicorn --log-file=- --worker-class gevent -w 1 -b 128.178.19.240:80 SmartBuilding.wsgi:application

#with more that 1 worker we have issues due to tcp daemon or 
#maybe now the localication as well
#gunicorn --debug --log-level debug --log-file=- --worker-class gevent -w 1 -b 128.178.19.240:80 SmartBuilding.wsgi:application
